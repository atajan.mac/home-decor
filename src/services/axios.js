import axios from "axios";

const config = {
  baseURL: "https://client.st.com.tm/api",
  withCredentials: true,
};

const axiosInstance = axios.create(config);

export default axiosInstance;
