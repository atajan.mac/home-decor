import { createRouter, createWebHistory } from "vue-router";
import axios from "axios";
import Home from "../components/Home.vue";
import Basket from "../components/Basket.vue";

import SetPageSeasons from "../components/setPages/SetPageSeasons.vue";
import SetPageNews from "../components/setPages/SetPageNews.vue";
import SetPageTops from "../components/setPages/SetPageTops.vue";
import SetPageRooms from "../components/setPages/SetPageRooms.vue";
import SetPageColors from "../components/setPages/SetPageColors.vue";
import SetPageNatures from "../components/setPages/SetPageNatures.vue";
import SetPageStyles from "../components/setPages/SetPageStyles.vue";
import SetPageFoodAndDrinks from "../components/setPages/SetPageFoodAndDrinks.vue";
import SetPageCitiesAndTechnology from "../components/setPages/SetPageCitiesAndTechnology.vue";
import SetPagePeopleAndRelationships from "../components/setPages/SetPagePeopleAndRelationships.vue";
import SetPagePostcards from "../components/setPages/SetPagePostcards.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  scrollBehavior(to, from, savedPosition) {
    return { top: 0 };
  },
  routes: [
    {
      path: "/",
      name: "Home",
      component: Home,
    },
    {
      path: "/seasons/:id",
      name: "setSeasonsPage",
      component: SetPageSeasons,
    },
    {
      path: "/new/:id",
      name: "setNewsPage",
      component: SetPageNews,
    },
    {
      path: "/top/:id",
      name: "setTopsPage",
      component: SetPageTops,
    },
    {
      path: "/room/:id",
      name: "setRoomsPage",
      component: SetPageRooms,
    },
    {
      path: "/color/:id",
      name: "setColorsPage",
      component: SetPageColors,
    },
    {
      path: "/nature/:id",
      name: "setNaturesPage",
      component: SetPageNatures,
    },
    {
      path: "/styles/:id",
      name: "setStylesPage",
      component: SetPageStyles,
    },
    {
      path: "/food-and-drink/:id",
      name: "setFoodAndDrinksPage",
      component: SetPageFoodAndDrinks,
    },
    {
      path: "/cities-and-technology/:id",
      name: "setCitiesAndTechnology",
      component: SetPageCitiesAndTechnology,
    },
    {
      path: "/people-and-relationships/:id",
      name: "setPeopleAndRelationships",
      component: SetPagePeopleAndRelationships,
    },
    {
      path: "/postcards/:id",
      name: "setPostcards",
      component: SetPagePostcards,
    },

    {
      path: "/basket",
      name: "basket",
      component: Basket,
    },
  ],
});

axios
  .get("https://client.st.com.tm/api/catalogs")
  .then((response) => {
    const routes = response.data.map((navItem) => ({
      path: `/${navItem.catalogURL}`,
      component: () => import(`../components/${navItem.catalogURL}.vue`),
      name: navItem.catalogURL,
      meta: { title: navItem.catalogTitle },
    }));

    // console.log("Routes from API:", routes);
    routes.forEach((route) => {
      router.addRoute(route);
    });
  })
  .catch((error) => {
    console.error("Error fetching navigation links:", error);
  });

export default router;
